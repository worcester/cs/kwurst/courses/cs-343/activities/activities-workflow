# In-Class Activities Workflow using Gitpod

We will be using [Gitpod](https://www.gitpod.io) as our development
environment because it runs in the cloud and does not require a local
computer with Docker and significant resources.

Gitpod will provide us with Visual Studio Code in the browser. But this will
also require being able to access projects on Gitlab.

These are the steps to follow to work on an in-class activity.

## Team Set-up

1. Determine who will take on each of the roles (Manager, Presenter, Recorder,
    Reflector) for this activity.
    - If this is the first activity of this class session (not continued from
        a previous class session), everyone should take a role that they have
        never played before, or have played least often.
    - If this activity is being continued from a previous class session, keep
        everyone in the same roles as the previous class session.
    - If you have already assigned new roles previously in this class session,
        keep everyone in the same roles as the previous activities.

The Recorder should:

  1. Connect their laptop to the projector.
  2. Open their browser.

Everyone else should be setting up, turning on, and
   connecting the projector for the team.

**If you do this as soon as you
   arrive to class, we will be able to get started right way on classwork.**
   You can chat as you do your team setup and after you are all set up.

## Make Sure the Recorder has a Gitpod Account (First Activity Only)

Each team member will have to do this eventually, but for this first time,
have just the Recorder do it.

### Create a Free Gitpod Account

1. Go to [https://gitpod.io/login](https://gitpod.io/login) and create a
free account.

### Gitpod Browser Extension

Gitpod provides a browser extension for Chrome or Firefox that adds a button
on the GitLab page in your browser.

1. Install the [Gitpod Browser Extension](https://www.gitpod.io/docs/configure/user-settings/browser-extension)

If you are not using Chrome or Firefox, you can still open GitLab projects
in your browser, but you will have to modify the URL to the project. You will
have to add `https://gitpod.io/#` at the beginning of the GitLab project URL.

## Make A Team Copy of the Activity (Fork)

You cannot make changes to the class copy of this project. You need to make
a *fork* of this project in a subgroup where you have the ability to make
changes.

The Recorder should:

1. Go to the In-Class Activities GitLab group. (There is a link in Blackboard).
2. Log in to GitLab.
3. Click on the project for the activity.
4. Click on the `Fork` button on the right-hand side (above Project
Information).
5. Under `Project URL` in the namespace dropdown choose your team's subgroup
it will look something like 
`worcester/cs/cs-XXX-YY-fall-ZZZZ/section-A/team-B`.
(Be sure to choose the correct section and team number.)
6. Click the `Fork project` button.

When it has finished, you will be on the GitLab page for your fork. Verify
this by looking at the address.

## Open the fork in Gitpod

1. Click on the `G Open` button to open the project in Gitpod.
    Or, paste `https://gitpod.io/#` at the beginning of the address in the
address bar.
2. Log in to GitLab is asked to do so.
3. When the `New Workspace` page is shown, click `Continue`.
4. You will see the project in the browser version of Visual Studio Code.

## Working in Visual Studio Code

Follow the instructions in the activity.

## Working in Markdown

Activities (and most text files in this class) are written in Markdown.

> Markdown is a lightweight markup language for creating formatted text using a plain-text editor.
>
> *Markdown. (2022, August 26). In Wikipedia. [https://en.wikipedia.org/w/index.php?title=Markdown&oldid=1106781712](https://en.wikipedia.org/w/index.php?title=Markdown&oldid=1106781712)*

You should be able to figure out how to write and edit Markdown from the examples files in class. But, if you want more details, see a [Markdown Cheat Sheet](https://devhints.io/markdown)

## Save Your Changes to GitLab

Your changes only exist in your Gitpod workspace at this point. The
workspace will be deleted after 14 days of not using it.

So, we should save your changes to your team's fork.

1. In your terminal type the following to add, commit, and push your work

    ``` bash
    git add .
    git commit -m"a message describing what you have done"
    git push
    ```

    Your changes will now be on GitLab.

2. Go to your fork on GitLab and refresh your browser. Verify that your
changes are there.

## Stop your Gitpod Workspace

You do not want to keep your Gitpod workspace running so that you can save
your free time. The workspace will shut down after 30 minutes of inactivity
but you can stop it immediately.

1. In your terminal type `gp stop`.

&copy; 2024 Karl R. Wurst, Worcester State University

<img src="http://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>
This work is licensed under the Creative Commons Attribution-ShareAlike 4.0
International License. To view a copy of this license, visit
[http://creativecommons.org/licenses/by-sa/4.0/]() or send a letter to
Creative Commons, 444 Castro Street, Suite 900, Mountain View, California,
94041, USA
